import React, { useState, useEffect } from 'react';
import Header from './components/Header';
import Figure from './components/Figure';
import WrongLetters from './components/WrongLetters';
import Word from './components/Word';
import Popup from './components/Popup';
import Notification from './components/Notification';
import { showNotification as show, checkWin } from './helpers/helpers';
import { ThemeProvider, createTheme } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { Card, CardContent, CardMedia, Switch, Typography } from "@mui/material"
import './App.css';

const words = ['application', 'programming', 'interface', 'wizard'];
let selectedWord = words[Math.floor(Math.random() * words.length)];

function App() {
    const [playable, setPlayable] = useState(true);
    const [correctLetters, setCorrectLetters] = useState([]);
    const [wrongLetters, setWrongLetters] = useState([]);
    const [showNotification, setShowNotification] = useState(false);

    // state to manage the dark mode
    const [toggleDarkMode, setToggleDarkMode] = useState(true);

    // function to toggle the dark mode as true or false
    const toggleDarkTheme = () => {
        setToggleDarkMode(!toggleDarkMode);
    };

    // applying the primary and secondary theme colors
    const darkTheme = createTheme({
        palette: {
            mode: toggleDarkMode ? 'dark' : 'light', // handle the dark mode state on toggle
            primary: {
                main: '#90caf9',
            },
            secondary: {
                main: '#131052',

            },
        },
    });
    useEffect(() => {
        const handleKeydown = event => {
            const { key, keyCode } = event;
            if (playable && keyCode >= 65 && keyCode <= 90) {
                const letter = key.toLowerCase();
                if (selectedWord.includes(letter)) {
                    if (!correctLetters.includes(letter)) {
                        setCorrectLetters(currentLetters => [...currentLetters, letter]);
                    } else {
                        show(setShowNotification);
                    }
                } else {
                    if (!wrongLetters.includes(letter)) {
                        setWrongLetters(currentLetters => [...currentLetters, letter]);
                    } else {
                        show(setShowNotification);
                    }
                }
            }
        }
        window.addEventListener('keydown', handleKeydown);

        return () => window.removeEventListener('keydown', handleKeydown);
    }, [correctLetters, wrongLetters, playable]);

    function playAgain() {
        setPlayable(true);

        // Empty Arrays
        setCorrectLetters([]);
        setWrongLetters([]);

        const random = Math.floor(Math.random() * words.length);
        selectedWord = words[random];
    }

    return (
        <>
            <ThemeProvider theme={darkTheme}>
                <CssBaseline/>
                <div className={toggleDarkMode ? 'dark-mode' : ''}
                     style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                        <h5>Dark Mode </h5>
                        <Switch checked={toggleDarkMode} onChange={toggleDarkTheme}/>
                    {/* rendering the card component with card content */}
                    <Header toggleDarkMode={toggleDarkMode}/>
                    <div className="game-container">
                        <Figure wrongLetters={wrongLetters} toggleDarkMode={toggleDarkMode}/>
                        <WrongLetters wrongLetters={wrongLetters} toggleDarkMode={toggleDarkMode}/>
                        <Word selectedWord={selectedWord} correctLetters={correctLetters}
                              toggleDarkMode={toggleDarkMode}/>
                    </div>
                    <Popup correctLetters={correctLetters} wrongLetters={wrongLetters} selectedWord={selectedWord}
                           setPlayable={setPlayable} playAgain={playAgain}/>
                    <Notification showNotification={showNotification}/>
                </div>
            </ThemeProvider>
        </>
    );
}

export default App;