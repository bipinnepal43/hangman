import React from 'react'

const WrongLetters = ({ wrongLetters,toggleDarkMode }) => {
    const textStyle = {
        color: toggleDarkMode ? 'white' : 'black',
    };
    return (
        <div className="wrong-letters-container">
            <div>
                {wrongLetters.length > 0 &&
                    <p style={textStyle}>Wrong</p>
                }
                {wrongLetters
                    .map((letter, i) => <span key={i} style={textStyle}>{letter }</span>)
                    .reduce((prev, curr) => prev === null ? [curr] : [prev, ', ', curr], null)}
            </div>
        </div>
    )
}

export default WrongLetters