import React from 'react';

const Word = ({ selectedWord, correctLetters,toggleDarkMode}) => {
    const letterStyle = {
        color: toggleDarkMode ? 'white' : 'black',
    };
    return (
        <div className="word">
            {selectedWord.split('').map((letter, i) => {
                return (
                    <span className="letter" key={i} style={letterStyle}>
            {correctLetters.includes(letter) ? letter : ''}
          </span>
                )
            })}
        </div>
    )
}

export default Word