import React from 'react'
import {Typography} from "@mui/material";

// rafce
const Header = ({toggleDarkMode}) => {
    return (
        <>
            <Typography variant="h5" style={{ color: toggleDarkMode ? 'white' : 'black' }}>
                Gidi Hang Man
            </Typography>
            <Typography variant="body1" style={{ color: toggleDarkMode ? 'white' : 'black' }}>
                Jhunidna bata bachau!
            </Typography>
        </>
    )
}

export default Header